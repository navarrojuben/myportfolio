
// ******************************************************--HEADER-LINKS
const headHomeLink      =   document.querySelector('.header-home-link   ');
const headAboutLink     =   document.querySelector('.header-about-link  ');
const headProjectLink   =   document.querySelector('.header-project-link');
const headContactLink =   document.querySelector('.header-contact-link');

const homeDiv = document.querySelector('#home');
homeDiv.addEventListener('mouseenter', setHeadHomeActive  );

const aboutDiv = document.querySelector('#about');
aboutDiv.addEventListener('mouseenter', setHeadAboutActive  );

const projectDiv = document.querySelector('#project');
projectDiv.addEventListener('mouseenter', setHeadProjectActive  );

const contactDiv = document.querySelector('#contact');
contactDiv.addEventListener('mouseenter', setHeadContactActive  );

headHomeLink   .addEventListener('click', setHeadHomeActive   );
headAboutLink  .addEventListener('click', setHeadAboutActive  );
headProjectLink.addEventListener('click', setHeadProjectActive);
headContactLink.addEventListener('click', setHeadContactActive);

function setHeadHomeActive(){
    resetHeadActive();
    headHomeLink.classList.add('header-active');
}

function setHeadAboutActive(){
    resetHeadActive();
    headAboutLink.classList.add('header-active');
}

function setHeadProjectActive(){
    resetHeadActive();
    headProjectLink.classList.add('header-active');
}

function setHeadContactActive(){
    resetHeadActive();
    headContactLink.classList.add('header-active');
}

function resetHeadActive(){
    headHomeLink   .classList.remove('header-active');
    headAboutLink  .classList.remove('header-active');
    headProjectLink.classList.remove('header-active');
    headContactLink.classList.remove('header-active');
}

// ******************************************************--HEADER-LINKS

// ******************************************************--FOOTER-LINKS
const homeLink     = document.querySelector('.home-link'   );
const aboutLink    = document.querySelector('.about-link'  );
const projectLink  = document.querySelector('.project-link');
const contactLink  = document.querySelector('.contact-link');

homeLink   .addEventListener('click', setHomeActive    );
aboutLink  .addEventListener('click', setAboutActive   );
projectLink.addEventListener('click', setProjectActive );
contactLink.addEventListener('click', setContactActive );

homeDiv   .addEventListener('mouseenter', setHomeActive     );
aboutDiv  .addEventListener('mouseenter', setAboutActive    );
projectDiv.addEventListener('mouseenter', setProjectActive  );
contactDiv.addEventListener('mouseenter', setContactActive  );

function setHomeActive(){
    resetActive();
    homeLink.classList.add('footer-active');
}

function setAboutActive(){
    resetActive();
    aboutLink.classList.add('footer-active');
}

function setProjectActive(){
    resetActive();
    projectLink.classList.add('footer-active');
}

function setContactActive(){
    resetActive();
    contactLink.classList.add('footer-active');
}


function resetActive(){
    homeLink.classList   .remove('footer-active');
    aboutLink.classList  .remove('footer-active');
    projectLink.classList.remove('footer-active');
    contactLink.classList.remove('footer-active');
}
// ******************************************************--FOOTER-LINKS


//******************************************************--HOME-TYPE-WRITER
// const TypeWriter = function(txtElement, words, wait = 3000){
//         this.txtElement = txtElement;
//         this.words = words;
//         this.txt = '';
//         this.wordIndex = 0;
//         this.wait = parseInt(wait, 10);
//         this.type();
//         this.isDeleting = false;
// }

// //Type Method
// TypeWriter.prototype.type = function(){

//     //Current index of word
//     const current = this.wordIndex % this.words.length;

//     //Get full text of current word
//     const fullTxt = this.words[current];
    
//     //Check if deleting
//     if(this.isDeleting){
//         //Remove char
//         this.txt = fullTxt.substring(0, this.txt.length - 1);
//     }

//     else{
//         //Add char
//         this.txt = fullTxt.substring(0, this.txt.length + 1);
//     }

//     // Insert txt into element
//     this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

//     //Initial Type Speed
//     let typeSpeed = 300;

//     if(this.isDeleting){
//         typeSpeed /= 2;
//     }

//     //If word is complete
//     if(!this.isDeleting && this.txt === fullTxt){
//         //Make pause at end
//         typeSpeed = this.wait;
//         //Set delete to true
//         this.isDeleting = true;
//     }
//     else if(this.isDeleting && this.txt === ''){
//         this.isDeleting = false;
//         //Move to Next word
//         this.wordIndex++;
//         //Pause before start typing
//         typeSpeed = 500;
//     }



//     setTimeout(() => this.type(), typeSpeed);
// }


// //Init On DOM Load
// document.addEventListener('DOMContentLoaded', init);

// // Init App
//     function init(){
//         const txtElement = document.querySelector('.txt-type');
//         const words = JSON.parse(txtElement.getAttribute('data-words'));
//         const wait = txtElement.getAttribute('data=wait');
//         //Init TypeWriter

//         new TypeWriter(txtElement, words, wait);
//     }

// ES6 Class
class TypeWriter{
    constructor(txtElement, words, wait = 3000){
    this.txtElement = txtElement;
    this.words = words;
    this.txt = '';
    this.wordIndex = 0;
    this.wait = parseInt(wait, 10);
    this.type();
    this.isDeleting = false;
    }

    type() {
         //Current index of word
    const current = this.wordIndex % this.words.length;

    //Get full text of current word
    const fullTxt = this.words[current];
    
    //Check if deleting
    if(this.isDeleting){
        //Remove char
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    }

    else{
        //Add char
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    // Insert txt into element
    this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

    //Initial Type Speed
    let typeSpeed = 300;

    if(this.isDeleting){
        typeSpeed /= 2;
    }

    //If word is complete
    if(!this.isDeleting && this.txt === fullTxt){
        //Make pause at end
        // typeSpeed = this.wait;
        typeSpeed = 3000;
        //Set delete to true
        this.isDeleting = true;
    }
    else if(this.isDeleting && this.txt === ''){
        this.isDeleting = false;
        //Move to Next word
        this.wordIndex++;
        //Pause before start typing
        typeSpeed = 500;
    }



    setTimeout(() => this.type(), typeSpeed);
    }
}

//Init On DOM Load
document.addEventListener('DOMContentLoaded', init);

// Init App
    function init(){
        const txtElement = document.querySelector('.txt-type');
        const words = JSON.parse(txtElement.getAttribute('data-words'));
        const wait = txtElement.getAttribute('data=wait');
        //Init TypeWriter

        new TypeWriter(txtElement, words, wait);
    }
//******************************************************--HOME-TYPE-WRITER


// ******************************************************--PROJECT-MODAL
const projectLinks = document.querySelectorAll('.project-links');
const closeProjectModalBtn = document.querySelector('.close-project-modal-btn');

openProjectModal();

closeProjectModalBtn.addEventListener('click', closeProjectModal);

function openProjectModal(){
    // sideMenuLinks.forEach(item => item.classList.add('show'));
    projectLinks.forEach(item => item.addEventListener(('click'), function openPrjModal(){
        if( !item.classList.contains('open-project-modal') ){
            item.classList.add('open-project-modal');
        }

        closeProjectModalBtn.classList.add('view-close-project-modal-btn');
    }));
}

function closeProjectModal(){
    projectLinks.forEach(item => item.classList.remove('open-project-modal'));
    closeProjectModalBtn.classList.remove('view-close-project-modal-btn');
}






// ******************************************************--PROJECT-MODAL




// ******************************************************--lOCATION-MODAL
const myLocatioNBtn   = document.querySelector('.my-location-btn  ');
const myLocationModal = document.querySelector('.my-location-modal');

const closeMyLocationBtn = document.querySelector('.close-location-modal-btn')


myLocatioNBtn.addEventListener('click', openMyLocationModal);
function openMyLocationModal(){
    myLocationModal.classList.add('open-location-modal');
}

closeMyLocationBtn.addEventListener('click', closeMyLocationModal);
function closeMyLocationModal(){
    myLocationModal.classList.remove('open-location-modal');
}

// ******************************************************--lOCATION-MODAL


